#include <guess/guess.hpp>

#include <iostream>
#include <cstdlib>
#include <time.h>
#include <array>
using namespace std;

int mul2(int n) {
    return n*2;
}

///Crée un nombre aléatoire entre 1 et 100
int rdNum ()
{
    srand((unsigned) time(0));
    return rand()%100+1;
}

///Affiche l'historique des valeurs que le joueur à rentré
void affHisto(int cmp, int myguess, int *tab){
    tab[cmp]=myguess;

    for(int i=0; i<=cmp; i++){
        if(tab[i]==0){
            cout<<"";
        }
        else{
          cout<<tab[i]<<" ";  
        }
    }
    cout<<endl;
}

//Demande au joueur un nombre, le compare avec le nombre nombre aléatoire "findme" puis affiche si la partie est win ou lose
void newgame (){
    int myguess;
    int findme = rdNum();
    int history[5];

    for(int i=0; i<5; i++){
        cout<<"history : ";
        affHisto(i, myguess, history);
        cout<<"number? ";
        
        cin>>myguess; 
        while(myguess<0 || myguess>100){
            cout<<"number?";
            cin>>myguess; 
        }
        
        if(myguess < findme){
            cout<<"too low"<< endl<< endl;
        }
        else{
            if(myguess > findme){
                cout<<"too high"<< endl<< endl;
            }
            else{
                cout<<"win"<< endl;
                return;
            }
        }  
    }
    cout<<"lose"<< endl;
    cout<<"target: "<<findme;
}